<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/28/16
 * Time: 5:24 AM
 */
?>
<!--app/views / eloquent . blade . php-->

<!doctype html >
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> Link checker </title>

    <!--CSS -->
    <!--BOOTSTRAP -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <script>
        function getItemInfo(id) {
            if(!$('#item-info-' + id).is(":visible")) {
                $('#item-info-' + id).show();
                $.get('/getInfo', {id: id}, function (data) {
                    $('#item-info-' + data.id).html(data.info)
                }, 'json')
            }
            else {
                $('#item-info-' + id).hide();
            }
        }
        function checker(id){
            $.get('/check/' + id,{},function(msg){
                $('#result-message').html(msg);
            })
        }

        function checkMultiple() {
//            var values = $('input:checkbox:checked.item-check').map(function () {
//                return this.value;
//            }).get()//.then($.post('/checkMultipe',values));
            $.post('/checkMultiple',{
                 ids:$('input:checkbox:checked.item-check').map(function () {
                     return this.value;
                 }).get(),
                _token:$('input[name="_token"]').attr('value')})
        }

        function selectAll(){
            var checked = $('#check-all').is(':checked');
            //console.log(checked);
            $('input:checkbox.item-check').each(function () {
                this.checked=checked;
            })
        }
    </script>
    <script>
        var socket = io('http://192.168.1.164:8081');
        var num = 1;
        socket.on('new link', function(msg){
            $('#result-message').html(num + ": " + msg);
            num++
        })
    </script>
    <style>
        body {
            padding-top: 15px;
        }

        #result-message {
            margin-top:5px;
            margin-bottom: 5px;
            background-color: #d7ebf6;
        }
        /* add some padding to the top of our site */
    </style>
</head>
<body class="container">
<div class="col-sm-12" id="result-message">
</div>
@if(Session::has('flash_message'))
    <div class="alert alert-success"> {{ Session::get('flash_message') }}</div>
@endif
<div class="col-sm-12 search-form">
    <form action="{{ url('/') }}" method="get">
        <input type="text" name='links' value="{{ $linkstr }}">
        <select name = "version">
            <option value=""></option>
            @foreach($versions as $version)
                @if( $versionstr == $version)
                    <option value="{{$version}}" selected> {{ $version }}</option>
                @else
                    <option value="{{$version}}"> {{ $version }}</option>
                @endif
            @endforeach
        </select>
        <button type="submit" class="btn">Search</button>
    {{ csrf_field() }}
    </form>
    </div>
<div class="col-sm-12">
    {{ csrf_field() }}
    <table style="width: 100%;">
        <tr>
            <th><input type="checkbox" id='check-all' onclick = "selectAll()"></th>
            <th width="75px">
                <a href="
                @if($order == 'id|asc')
                    {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'id|desc'])}}
                @else
                    {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'id|asc'])}}
                @endif
                    ">
                ID
                    @if($order == 'id|asc')
                        <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    @elseif($order == 'id|desc')
                        <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                    @endif
                    </a>
            </th>
            <th>
                <a href="
                @if($order == 'link|asc')
                {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'link|desc'])}}
                @else
                {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'link|asc'])}}
                @endif
                        ">
                    Link URL
                    @if($order == 'link|asc')
                        <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    @elseif($order == 'link|desc')
                        <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                    @endif
                    </a>
            </th>
            <th width="150px">
                <a href="
                @if($order == 'version|asc')
                {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'version|desc'])}}
                @else
                {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'version|asc'])}}
                @endif
                        ">
                    Version
                    @if($order == 'version|asc')
                        <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    @elseif($order == 'version|desc')
                        <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                    @endif
                </a>
            </th>
            <th width="50px">
                <a href="
                @if($order == 'elapsed_time|asc')
                {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'elapsed_time|desc'])}}
                @else
                {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'elapsed_time|asc'])}}
                @endif
                        ">
                    Spend time
                    @if($order == 'elapsed_time|asc')
                        <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    @elseif($order == 'elapsed_time|desc')
                        <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                    @endif
                </a>

            </th>
            <th width="150px">
                <a href="
                @if($order == 'updated_at|asc')
                {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'updated_at|desc'])}}
                @else
                {{url('/') . '?' . http_build_query(['version'=>$versionstr,'links'=>$linkstr, 'order'=>'updated_at|asc'])}}
                @endif
                        ">
                    Updated
                    @if($order == 'updated_at|asc')
                        <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    @elseif($order == 'updated_at|desc')
                        <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                    @endif
                </a>
            </th>
            <th>check</th>
        </tr>
        @foreach ($links as $link)
            <tr>
                <td style="vertical-align: top;"><input type="checkbox" class="item-check" value="{{$link->id}}" name="ids[]"></td>
                <td style="vertical-align: top;">{{$link->id }}</td>
                <td style="vertical-align: top;">
                    <a style="display: block; width:auto; float:left;" href="{{$link->link}}" target="_blank">{{$link->link}}</a>
                    <div style="width: 125px; float: right; margin-right: 10px;cursor:pointer;" onclick="getItemInfo({{$link->id}});return false;">Additional info<span class="caret"></span></div>
                    <div id="item-info-{{$link->id}}" style="clear:both;display:none;width:100%; ">
                        <img src="{{ asset('img/ajax-loader.gif') }}">
                    </div>
                </td>
                <td style="vertical-align: top;">{{$link->version}}</td>
                <td style="vertical-align: top;">{{ round($link->elapsed_time, 2) }}s</td>
                <td style="vertical-align: top;">{{$link->updated_at}}</td>
                <td style="vertical-align: top;"><a href="/check/{{$link->id}}" onClick="checker({{$link->id}});return false;">check</a></td>
            </tr>
        @endforeach
    </table>
</div>
<div class="col-sm-12">
    <button onclick="checkMultiple();" class="btn">Check selected</button>
    <a href="{{ url('/checkUndef') }}" class="btn btn-info" title="check all links">Check undefined</a>
    <a href="{{ url('/checkAll') }}" class="btn btn-info" title="check all links">Check all({{$number}})</a>
    <a class="btn btn-info" href="{{ url('/export') }}">Export</a>
    <a class="btn btn-info" href="{{ url('/upload') }}">Import </a>
</div>

<div class="col-sm-12">
    {{ $links->appends('version', $versionstr)->appends('links',$linkstr)->links() }}
</div>
</body>
</html>