<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/13/17
 * Time: 10:06 AM
 */
?>
<table width="100%">
    <tr>
        <th>Date</th>
        <th>Version</th>
        <th>Spend time</th>
    </tr>
    @foreach($revisions as $item)
        <tr>
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->version }}</td>
            <td>{{ round($item->elapsed_time, 2) }}s</td>
        </tr>
    @endforeach
</table>
