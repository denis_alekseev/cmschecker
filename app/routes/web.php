<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Links@index');

Route::get('upload', function () {
  return View::make('pages.upload');
});

Route::post('apply/upload', 'Links@upload');

Route::get('check/{id}', 'Links@check');

Route::post('checkMultiple', 'Links@checkMultiple');

Route::get('checkAll', 'Links@checkAll');

Route::get('export', 'Links@export');

Route::get('sandbox', 'Links@sandbox');

Route::get('checkUndef', 'Links@checkUndefined');

Route::get('getInfo', 'Links@getInfo');

