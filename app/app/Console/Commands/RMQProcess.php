<?php

namespace App\Console\Commands;

use App\Revision;
use Illuminate\Console\Command;
use \PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\LinksModel;

class RMQProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RMQ:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update information in database from RabbitMQ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $connection =  new AMQPStreamConnection('localhost','5672','guest','guest');
      $chanel = $connection->channel();
      echo "links_r channel listening...\n";
      $chanel->queue_declare('links_r', false, false, false, false);
      $chanel->queue_declare('links_n', false, false, false, false);

      $cb = function($msg) use ($chanel) {
        $tmp = explode('|',$msg->body);
        $obj = LinksModel::find($tmp[0]);
        $obj->version = str_replace(',','',$tmp[1]);
        $obj->elapsed_time = $tmp[2];
        $obj->save();
        $rev = Revision::create(['lid' => $tmp[0], 'version' => $tmp[1],'elapsed_time'=>$tmp[2]]);
        $rev->save();
        $msg = new AMQPMessage("Site ". $obj->link . " have Drupal version " . $obj->version);
        $chanel->basic_publish($msg,'', 'links_n');
        echo date('d-m-Y H:i:s') . ' : ' . $obj->link . ' - ' . $obj->version . "\n";
      };

      $chanel->basic_consume('links_r', '', false, true, false, false, $cb);
      while(count($chanel->callbacks)) {
       // echo count($chanel->callbacks);
        $chanel->wait();
      }
    }
}
