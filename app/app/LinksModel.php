<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinksModel extends Model
{

    //
  public $table='links';

  protected $fillable = [
    'link', 'version','elapsed_time'
  ];

  protected $casts = [
    'id'=> 'integer',
    'link'=>'string',
    'version' =>'string',
    'elapsed_time'=>'float',
  ];

}
