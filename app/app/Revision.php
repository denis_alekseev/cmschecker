<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    //
  public $table='revisions';

  protected $fillable = [
    'lid', 'version','elapsed_time'
  ];

  protected $casts = [
    'id'=> 'integer',
    'lid'=>'integer',
    'version' =>'string',
    'elapsed_time'=>'float',
  ];
}
