<?php

namespace App\Http\Controllers;

use App\LinksModel;
use App\Revision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use \PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Links extends Controller {
  //
  private $connection;
  private $chanel;

  public function __construct() {
    $this->connection =  new AMQPStreamConnection('localhost','5672','guest','guest');
    $this->chanel = $this->connection->channel();
    $this->chanel->queue_declare('links_q', false, false, false, false);
  }

  public function index(Request $request) {

    $number = \DB::table('links')->count();
    $order = (Input::has('order'))?Input::get('order'):'id|asc';

    $query = LinksModel::query();

    if(Input::has('version')) {
      $query->where('version', Input::get('version'));
    }

    if(Input::has('links')) {
      $query->where('link','like','%' . Input::get('links') . '%');
    }

    $tmp = explode('|',$order);
    $query->orderBy($tmp[0],$tmp[1]);

    $links = $query->paginate(100);
    $versions = \DB::table('links')->select('version')->distinct()->orderBy('version','desc')->get();
    $v = [];
    foreach ($versions as $version)
      $v[]=$version->version;

    return View::make('links')
      ->with('links',$links)
      ->with('versions',$v)
      ->with('linkstr',Input::get('links'))
      ->with('versionstr',Input::get('version'))
      ->with('number', $number)
      ->with('order',$order);
  }

  public function upload(Request $request) {

    $this->validate($request, [
      'file' => 'required|file',
    ]);
    $res = $this->insertLinks($request->file->getPathName());

    \Session::flash('flash_message', $res . ' links was added to database');
    return redirect('/');
  }

  public function check($id) {
    $link = LinksModel::find($id);

    $msg = new AMQPMessage($link->id . "|" .$link->link);
    $this->chanel->basic_publish($msg,'', 'links_q');

    //\Session::flash('flash_message', $link->link . ' was added to Check queue');
    return $link->link . ' was added to Check queue';

  }

  public function checkMultiple(Request $request) {
    $links = LinksModel::find($request->ids);
    foreach($links as $link) {
      $msg = new AMQPMessage($link->id . "|" .$link->link);
      $this->chanel->basic_publish($msg,'', 'links_q');
    }
  }

  public function  checkAll() {
    $links =  LinksModel::get();


    foreach($links as $link) {
      $msg = new AMQPMessage($link->id . "|" .$link->link);
      $this->chanel->basic_publish($msg,'', 'links_q');
    }
    \Session::flash('flash_message', 'Base reindex was started');
    return redirect('/');
  }

  public function checkUndefined()
  {
    $links =  LinksModel::where('version','undefined')->get();


    foreach($links as $link) {
      $msg = new AMQPMessage($link->id . "|" .$link->link);
      $this->chanel->basic_publish($msg,'', 'links_q');
    }
    \Session::flash('flash_message', 'Base reindex was started');
    return redirect('/');
  }

  private function insertLinks($file) {

    $f = fopen($file, 'r');
    $data = fgets($f, 1000); // Ignore header
    $num = 0;
    $links = $this->getAllLinks();
    while ($data = fgets($f, 1000)) {
      $data = explode(',',$data);

      if(!in_array($data[0],$links)) {
        $link = LinksModel::create([
          'link' => $data[0],
          'version' => ($data[2] == '' || $data[2] == 'undefined')?'undefined':$data[1] . ' ' . $data[2],
        ]);
        $link->save();
        $num++;
      }
    }
    return $num;
  }

  public function export() {
    $links =  LinksModel::get();
    $file = fopen(storage_path() . '/my.csv','w');

    fputs($file,'url,system,version' . "\n"); // write header

    foreach($links as $link) {
      $res=[$link->link];
      if($link->version == 'undefined' || $link->version == '') {
        $res[] = 'Drupal';
        $res[] = 'undefined';
      }
      else
      {
        $link->version = str_replace(' ',',', $link->version);
        $res[] = ($link->version[strlen($link->version)-1] == ',')?substr($link->version,0,strlen($link->version)-1):$link->version;
      }
      fputs($file,implode(',',$res) . "\n");
    }
    fclose($file);
  }

  public function getInfo() {
    $id = Input::get('id');
    $link = LinksModel::find($id);
    $revision = Revision::where('lid',$id)->orderBy('created_at','desc')->get();
    $view = View::make('iteminfo')
      ->with('link',$link)
      ->with('revisions',$revision);
    return json_encode(['id'=>$id, 'info'=>(string)$view->render()],JSON_FORCE_OBJECT);
  }

  private function getAllLinks()
  {
    $res = [];
    $links = \DB::table('links')->select('link')->get(['link']);
    foreach($links as $item)
      $res[] = $item->link;
    return $res;
  }

  public function sandbox() {
    $revision = Revision::create(['lid' => 1, 'version' => '213']);
    $revision->save();
//    print_r(LinksModel::with(['link' => 'alchemist-home.ru'])->take(1)->get());
  }
}
