package main

import (
	"log"
	"net/http"
	"io/ioutil"
//	"fmt"
	"strings"
	"github.com/streadway/amqp"
	"regexp"
	"sync"
	"crypto/tls"
	"time"
	"strconv"
)

var ch *amqp.Channel
var resp *amqp.Queue

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"links_q", // name
		false,   // durable
		false,   // delete when usused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	resp, err := ch.QueueDeclare(
		"links_r", // name
		false,   // durable
		false,   // delete when usused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			t0 := time.Now()
			log.Printf("Received a message: %s", d.Body)
			msg := string(d.Body);
			id,link := splitLink(msg,"|")
			log.Printf(id)
			changelog := link + "/CHANGELOG.txt"
			res, err := client.Get(changelog)
			if err!=nil || res.StatusCode >= 400 {
				checkVersion(id, link,ch,t0)
			} else {
				reg, _ := regexp.Compile("Drupal ([0-9.]+),")
				change, _ := ioutil.ReadAll(res.Body)
				num := reg.FindString(string(change))
				if(num == "") {
					checkVersion(id, link,ch,t0)
				} else {
					t1:= time.Now()
					ch.Publish(
						"",
						resp.Name,
						false,
						false,
						amqp.Publishing{
							ContentType: "text/plain",
							Body:        []byte(id + "|" + num + "|" + strconv.FormatFloat(t1.Sub(t0).Seconds(),'f',-3,64)),
						})
					log.Printf(id + " " + num)
					res.Body.Close()
				}
			}
			d.Ack(false)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func checkVersion(id, link string, ch *amqp.Channel, t0 time.Time) {
	message := make(chan string)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		defer wg.Done()
		log.Printf("is Drupal 8")
		res, err := client.Get(link + "/core/misc/ajax.js")
		if err == nil && res.StatusCode < 400 {
			r, _ := ioutil.ReadAll(res.Body)
			match,_ := regexp.MatchString(`Drupal\.behaviors\.AJAX`, string(r))
			if match {
				match, _ := regexp.MatchString(`if \(ajaxSettings\)`, string(r))
				if match {
					message <-id + "|Drupal 8.2.0-8.2.1"
				} else {
					message <-id + "|Drupal 8.0.0-8.1.10"
				}
			}
			defer res.Body.Close()
		}
	}()

	go func() {
		defer wg.Done()
		log.Printf("is Drupal 7")
		res, err := client.Get(link + "/misc/ajax.js")

		if err == nil && res.StatusCode < 400 {
			r, _ := ioutil.ReadAll(res.Body)
			match, _ := regexp.MatchString(`Drupal\.ajax`, string(r))
			if match {
				match, _ := regexp.MatchString(`Drupal\.displayAjaxError`, string(r))
				if match {
					message <-id + "|Drupal 7.51"
				} else {
					message <-id + "|Drupal 7.0-7.50"
				}
			}
			defer res.Body.Close()
		}

	}()

	go func() {
		defer wg.Done()
		log.Printf("is Drupal 6")
		res, err := client.Get(link + "/modules/system/system.js")
		if err == nil && res.StatusCode < 400 {
			r, _ := ioutil.ReadAll(res.Body)
			match, _ := regexp.MatchString(`clean\-url\.clean\-url\-processed`, string(r))
			if match {
				match, _ := regexp.MatchString(`\/\\?\/`, string(r))
				if match {
					message <-id + "|Drupal 6.38"
				} else {
					message <-id + "|Drupal 6.0-6.37"
				}
			}
			defer res.Body.Close()
		}
	}()

	go func() {
		for i := range message {
			t1 := time.Now();
			ch.Publish(
				"",
				"links_r",
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        []byte(i + "|" + strconv.FormatFloat(t1.Sub(t0).Seconds(),'f',-3,64)),
				})

			log.Printf(id + " " + i)

		}
	}()
	wg.Wait()

}

func splitLink(s, sep string) (string, string) {

    x := strings.Split(s, sep)
    return x[0], x[1]
}
