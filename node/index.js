/**
 * Created by user on 1/8/17.
 */
const express = require('express');
const app = express();
const http =require('http').Server(app);
const io = require('socket.io')(http);
const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        if(err) {
            console.log(err);
        }

        var q = 'links_n';

        ch.assertQueue(q, {durable: false});
        ch.consume(q, function(msg) {
            io.emit("new link", msg.content.toString());
            console.log(" [x] Received %s", msg.content.toString());
        }, {noAck: true});

    });
});

var server = http.listen(8081, function () {
 var host = server.address().address;
 var port = server.address().port;

   console.log("Example app listening at http://%s:%s", host, port)
});